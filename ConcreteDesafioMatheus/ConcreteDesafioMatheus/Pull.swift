//
//  Pull.swift
//  ConcreteDesafioMatheus
//
//  Created by Matheus Nishi on 21/09/17.
//  Copyright © 2017 Matheus Nishi. All rights reserved.
//

import Foundation

import ObjectMapper

class Pull: Mappable {
    
    var title: String!
    var description: String!
    var user: User!
    var createdAt: String!
    var htmlUrl: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        title <- map["title"]
        description <- map["description"]
        user <- map["user"]
        createdAt <- map["created_at"]
        htmlUrl <- map["html_url"]
    }
    
}
