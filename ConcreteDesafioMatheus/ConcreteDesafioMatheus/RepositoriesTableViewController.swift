//
//  RepositoriesTableViewController.swift
//  ConcreteDesafioMatheus
//
//  Created by Matheus Nishi on 20/09/17.
//  Copyright © 2017 Matheus Nishi. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

class RepositoriesTableViewController: UITableViewController {
    
    let downloader = ImageDownloader()
    
    var isLoading = false
    var pageNumber = 1
    var repositories = [Repository]() {
        didSet {
            let hasItems = repositories.count > 0
            tableView.isHidden = !hasItems
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        loadRepositories()


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return repositories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepositoriesTableViewCell.identifier, for: indexPath) as! RepositoriesTableViewCell
        
        let repository = repositories[indexPath.row]
        
        cell.nameRepositoryLabel.text = repository.nameRepository
        cell.descripitionRepositoryLabel.text = repository.descriptionRepository
        cell.forksCountLabel.text = "\(repository.forksCount)"
        cell.starsCountLabel.text = "\(repository.starsCount)"
        
        downloadAvatar(indexPath)
        
        cell.userNameLabel.text = repository.name.login
        
        if let name = repository.name.name {
            cell.nameLabel.text = name
        } else {
            downloadInfoUser(indexPath)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (indexPath) in
            downloadAvatar(indexPath)
            downloadInfoUser(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (indexPath) in
            cancelDownloadAvatar(indexPath)
            cancelDownloadInfoUser(indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == repositories.count - 5 {
            if !isLoading {
                loadRepositories()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: PullsTableViewController.identifier, sender: self)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == PullsTableViewController.identifier {
            if let pullsViewController = segue.destination as? PullsTableViewController {
                pullsViewController.repositoryFullName = repositories[tableView.indexPathForSelectedRow!.row].fullName
            }
        }
    }
    
    //MARK: - Request
    
    func loadRepositories() {
        Alamofire.request(Route.searchRepositories.replacingOccurrences(of: "{page}", with: "\(pageNumber)")).responseObject { (response: DataResponse<Search>) in
            if let searchResponse = response.result.value {
                if let respositories = searchResponse.repositories {
                    self.repositories += respositories
                    self.pageNumber += 1
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    //MARK: - Info User Download
    
    func downloadInfoUser(_ indexPath: IndexPath) {
        let repository = repositories[indexPath.row]
        
        Alamofire.request(repository.name.url + Param.clientAndSecret).responseObject { (response: DataResponse<User>) in
            if let owner = response.result.value {
                repository.name.name = owner.name
                if let cell = self.tableView.cellForRow(at: indexPath) as? RepositoriesTableViewCell {
                    if let name = owner.name {
                        cell.userNameLabel.text = name
                    }
                }
            }
        }
    }
    
    func cancelDownloadInfoUser(_ indexPath: IndexPath) {
        let repository = repositories[indexPath.row]
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach({ (task) in
                if task.originalRequest?.url?.absoluteString == repository.name.url {
                    task.cancel()
                }
            })
        }
    }
    
    //MARK: - Image Download
    
    /*
     The ImageDownloader uses a combination of an URLCache and AutoPurgingImageCache
     to create a very robust, high performance image caching system.
     */
    
    func downloadAvatar(_ indexPath: IndexPath) {
        let repository = repositories[indexPath.row]
        let urlRequest = URLRequest(url: URL(string: repository.name.avatarImage)!)
        
        downloader.download(urlRequest) { (response) in
            if let image = response.result.value {
                if let cell = self.tableView.cellForRow(at: indexPath) as? RepositoriesTableViewCell {
                    cell.avatarImage.image = image
                }
            }
        }
    }
    
    func cancelDownloadAvatar(_ indexPath: IndexPath) {
        let url = repositories[indexPath.row].name.avatarImage
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach({ (task) in
                if task.originalRequest?.url?.absoluteString == url {
                    task.cancel()
                }
            })
        }
    }
    
}
