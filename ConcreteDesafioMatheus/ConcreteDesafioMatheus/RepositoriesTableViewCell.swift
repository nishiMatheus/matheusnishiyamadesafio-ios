//
//  RepositoriesTableViewCell.swift
//  ConcreteDesafioMatheus
//
//  Created by Matheus Nishi on 20/09/17.
//  Copyright © 2017 Matheus Nishi. All rights reserved.
//

import UIKit

class RepositoriesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameRepositoryLabel: UILabel!
    @IBOutlet weak var descripitionRepositoryLabel: UILabel!
    @IBOutlet weak var forksCountLabel: UILabel!
    @IBOutlet weak var starsCountLabel: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    static let identifier = "Repository"

    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.text = " "
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

        // Configure the view for the selected state
    }

}
