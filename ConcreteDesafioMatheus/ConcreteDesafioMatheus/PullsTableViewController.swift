//
//  PullsTableViewController.swift
//  ConcreteDesafioMatheus
//
//  Created by Matheus Nishi on 21/09/17.
//  Copyright © 2017 Matheus Nishi. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

class PullsTableViewController: UITableViewController {
    
    static let identifier = "Pull"

    
    let downloader = ImageDownloader()
    
    var repositoryFullName: String!
    var pulls = [Pull]() {
        didSet {
            let hasItems = pulls.count > 0
            tableView.isHidden = !hasItems
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        loadPulls()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return pulls.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: PullsTableViewCell.identifier, for: indexPath) as! PullsTableViewCell
        
        let pull = pulls[indexPath.row]
        
        cell.title.text = pull.title
        cell.descriptionPullLabel.text = pull.description
        
        downloadAvatar(indexPath)
        
        cell.usernamePullLabel.text = pull.user.login
        
        if let name = pull.user.name {
            cell.namePullLabel.text = name
        } else {
            downloadInfoUser(indexPath)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pull = pulls[indexPath.row]
        if let url = URL(string: pull.htmlUrl) {
            UIApplication.shared.open(url)
        }
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (indexPath) in
            downloadAvatar(indexPath)
            downloadInfoUser(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (indexPath) in
            cancelDownloadAvatar(indexPath)
            cancelDownloadInfoUser(indexPath)
        }
    }
    
    //MARK: - Request
    
    func loadPulls() {
        Alamofire.request(Route.pulls.replacingOccurrences(of: "{fullName}", with: repositoryFullName) + Param.clientAndSecret).responseArray { (response: DataResponse<[Pull]>) in
            if let pulls = response.result.value {
                self.pulls = pulls
                self.tableView.reloadData()
            }
        
        }
    }
    
    //MARK: - Name User Download
    
    func downloadInfoUser(_ indexPath: IndexPath) {
        let pull = pulls[indexPath.row]
        
        Alamofire.request(pull.user.url + Param.clientAndSecret).responseObject { (response: DataResponse<User>) in
            if let user = response.result.value {
                pull.user.name = user.name
                if let cell = self.tableView.cellForRow(at: indexPath) as? PullsTableViewCell {
                    if let name = user.name {
                        cell.usernamePullLabel.text = name
                    }
                }
            }
        }
    }
    
    func cancelDownloadInfoUser(_ indexPath: IndexPath) {
        let pull = pulls[indexPath.row]
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach({ (task) in
                if task.originalRequest?.url?.absoluteString == pull.user.url {
                    task.cancel()
                }
            })
        }
    }
    
    //MARK: - Image Download
    
    func downloadAvatar(_ indexPath: IndexPath) {
        let pull = pulls[indexPath.row]
        let urlRequest = URLRequest(url: URL(string: pull.user.avatarImage)!)
        
        downloader.download(urlRequest) { (response) in
            if let image = response.result.value {
                if let cell = self.tableView.cellForRow(at: indexPath) as? PullsTableViewCell {
                    cell.avatar.image = image
                }
            }
        }
    }
    
    func cancelDownloadAvatar(_ indexPath: IndexPath) {
        let url = pulls[indexPath.row].user.avatarImage
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach({ (task) in
                if task.originalRequest?.url?.absoluteString == url {
                    task.cancel()
                }
            })
        }
    }
    
}
