//
//  Route.swift
//  ConcreteDesafioMatheus
//
//  Created by Matheus Nishi on 21/09/17.
//  Copyright © 2017 Matheus Nishi. All rights reserved.
//

import Foundation

class Route {
    
    private static let mainUrl = "https://api.github.com"
    
    static var searchRepositories = mainUrl + "/search/repositories?q=language:Java&sort=stars&page={page}"
    static var pulls = mainUrl + "/repos/{fullName}/pulls"
    
}

class Param {
    
    static var clientAndSecret = "?client_id=5af9b43fd179b5b92542&client_secret=3e9952bd25cf67f9fdb1ce0245b58a52ec418364"
    
}
