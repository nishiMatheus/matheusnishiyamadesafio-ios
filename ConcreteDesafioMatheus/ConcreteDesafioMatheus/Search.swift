//
//  Search.swift
//  ConcreteDesafioMatheus
//
//  Created by Matheus Nishi on 21/09/17.
//  Copyright © 2017 Matheus Nishi. All rights reserved.
//

import Foundation

import ObjectMapper

class Search: Mappable {
    
    var repositories: [Repository]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        repositories <- map["items"]
    }
    
}
