//
//  PullsTableViewCell.swift
//  ConcreteDesafioMatheus
//
//  Created by Matheus Nishi on 21/09/17.
//  Copyright © 2017 Matheus Nishi. All rights reserved.
//

import UIKit


class PullsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionPullLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var usernamePullLabel: UILabel!
    @IBOutlet weak var namePullLabel: UILabel!
    
        static let identifier = "Pull"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
