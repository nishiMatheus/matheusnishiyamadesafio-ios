//
//  Repository.swift
//  ConcreteDesafioMatheus
//
//  Created by Matheus Nishi on 21/09/17.
//  Copyright © 2017 Matheus Nishi. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository: Mappable {
    
    var id: Int!
    var nameRepository: String!
    var fullName: String!
    var descriptionRepository: String?
    var forksCount: Int = 0
    var starsCount: Int = 0
    var name: User!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        nameRepository <- map["name"]
        fullName <- map["full_name"]
        descriptionRepository <- map["description"]
        forksCount <- map["forks_count"]
        starsCount <- map["stars_count"]
        name <- map["owner"]
    }
    
}
