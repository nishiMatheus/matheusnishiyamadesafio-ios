//
//  User.swift
//  ConcreteDesafioMatheus
//
//  Created by Matheus Nishi on 20/09/17.
//  Copyright © 2017 Matheus Nishi. All rights reserved.
//
import Foundation
import ObjectMapper

class User: Mappable {
    
    var login: String!
    var url: String!
    var name: String?
    var avatarImage: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        url <- map["url"]
        login <- map["login"]
        name <- map["name"]
        avatarImage <- map["avatar_url"]
    }
    
    
}
